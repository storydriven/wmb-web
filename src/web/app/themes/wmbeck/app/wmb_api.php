<?php

// /locations/change
//
add_action('rest_api_init', function () {
  register_rest_route( 'wmb/v1', 'locations/change', array(
    'methods'  => 'GET',
    'callback' => 'wmb_change_location'
  ));
});

function wmb_change_location(WP_REST_Request  $request) {
  $change_to_location = '';

  // Check if REST Request contains changeToLocation
  if ( $request['changeToLocation'] ) {
    $change_to_location = $request['changeToLocation'];
  } else {
    return new WP_Error( 'no_location_requested', 'Request does not contain changeToLocation', array('status' => 404) );
  }

  // Check if a location exists with requested slug
  $location_posts = new WP_Query( array(
    'post_type'       => 'location',
    'name'            => $change_to_location,
    'posts_per_page'  => 1,
  ) );

  if ( !$location_posts->have_posts() ) {
    return new WP_Error( 'location_not_found', 'No location was found with that name', array('status' => 404) );
  }

  $response = new WP_REST_Response($change_to_location, 200);

  return $response;
}
