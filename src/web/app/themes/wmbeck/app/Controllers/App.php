<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function siteUrl()
    {
        return get_bloginfo('url');
    }

    // Returns the WMB branch info shown in header.blade.php
    public function locationInfo()
    {
      $location_info_data = get_field('location_info', wmb_get_current_branch_id() );

      // Build URI for Google Maps directions to branch address
      $address_url_encode = str_replace('#','',$location_info_data['address']);
      $address_url_encode = urlencode( strip_tags( $address_url_encode ) );
      $directions_link_uri = 'https://www.google.com/maps?saddr=My+Location&daddr=' . $address_url_encode;

      $toll_free = ( $location_info_data['toll_free_number'] ) ? $location_info_data['toll_free_number'] : '(888) 437-1100';

      // Email defaults to wecare@wmbeck.com if branch does not have its own
      $email = ( $location_info_data['email_address'] ) ? $location_info_data['email_address'] : 'wecare@wmbeck.com';
      $branch_is_open = wmb_branch_is_open(wmb_get_current_branch_id());

      return array(
        'name'                => get_the_title( wmb_get_current_branch_id() ),
        'address'             => $location_info_data['address'],
        'address_single_line' => strip_tags( $location_info_data['address'] ),
        'directions_uri'      => $directions_link_uri,
        'hours'               => $location_info_data['hours']['hours_simple'],
        'phone'               => $location_info_data['phone_number'],
        'toll_free'           => $toll_free,
        'fax'                 => $location_info_data['fax_number'],
        'email'               => $email,
        'is_open'             => $branch_is_open['is_open'],
        'next_open_close'     => $branch_is_open['next_open_close']
      );
    }

    public function locationPopularCoverages()
    {
      $popular_coverage_ids = get_field('insurance_coverage', wmb_get_current_branch_id())['popular_coverages'];
      $popular_coverages_data = array();

      foreach ($popular_coverage_ids as $coverage_id) {
        $thumbnail_url = get_the_post_thumbnail_url($coverage_id);
        $fallback_thumbnail_url = \App\asset_path('images/FPO_800x600.png');
        $thumbnail = ( $thumbnail_url ) ? $thumbnail_url : $fallback_thumbnail_url;

        $coverage_data = array(
          'id' => $coverage_id,
          'thumbnail' => $thumbnail,
          'category' => get_coverage_terms_string($coverage_id, ' | '),
          'name' => get_the_title($coverage_id),
          'short_description' => get_field('short_description', $coverage_id),
          'long_description' => get_field('long_description', $coverage_id),
          'example_claim' => get_field('example_claim', $coverage_id)
        );
        array_push($popular_coverages_data, $coverage_data);
      }

      return $popular_coverages_data;
    }

    public function thisYear()
    {
      return date('Y');
    }

    public function breakpoints()
    {
      return array(
        'sm' => 576,
        'md' => 768,
        'lg' => 992,
        'xl' => 1200,
        'xxl' => 1400,
        'xxxl' => 1520
      );
    }


    // Global components

    public function hero()
    {
      return get_field('hero_section',get_the_ID());
    }

    public function intro()
    {
      return get_field('intro_section',get_the_ID());
    }

    public function callToAction()
    {
      return get_field('call_to_action',get_the_ID());
    }

    public function testimonials()
    {
      $testimonial_data = get_field('testimonials','option');
      $testimonials = array();

      foreach ($testimonial_data as $testimonial) {
        $attribution = $testimonial['name'];
        if ( $testimonial['title'] )
          $attribution .= ', ' . $testimonial['title'];
        if ( $testimonial['company_name'] )
          $attribution .= ', ' . $testimonial['company_name'];

        $testimonial_array = array(
          'logo' => $testimonial['logo'],
          'quote' => $testimonial['quote'],
          'attribution' => $attribution
        );

        array_push($testimonials, $testimonial_array);
      }

      return $testimonials;
    }


    // Site-wide modals

    public function changeLocationModalOptions()
    {
      $args = array(
        'post_type' => 'location',
        'numberposts' => -1
      );

      $locations = get_posts($args);
      $location_modal_options = array();

      foreach ($locations as $location) {
        $location_data = get_field('location_info', $location->ID);
        $address = strip_tags($location_data['address']);
        $name = get_the_title($location->ID);
        $slug = get_post_field('post_name', $location->ID);
        $branch_is_open = wmb_branch_is_open($location->ID);

        $hours_info = ( $branch_is_open['is_open'] ) ? 'Open: ' : 'Closed: ';
        $hours_info .= $branch_is_open['next_open_close'] . ' ' . strtoupper($location_data['hours']['time_zone']);

        $location_array = array(
          'name'              => $name,
          'slug'              => $slug,
          'address'           => $address,
          'hours'             => $hours_info,
          'phone'             => $location_data['phone_number'],
          'selected'          => ( $location->ID === wmb_get_current_branch_id() )
        );
        array_push( $location_modal_options, $location_array );
      }

      // die( var_dump( json_encode($location_modal_options) ) );

      return $location_modal_options;
    }


    // Footer Fields

    public function socialLinks()
    {
      return get_field('social_links','option');
    }

    public function footerPartnerCompanies()
    {
      return get_field('partner_companies','option');
    }

  }
