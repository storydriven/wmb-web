<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageCareers extends Controller
{
    public function wmbDifference()
    {
        return get_field('wmb_difference_section');
    }

    public function teamTestimonials()
    {
        return get_field('team_testimonials');
    }
}
