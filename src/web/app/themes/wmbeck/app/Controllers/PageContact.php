<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageContact extends Controller
{
  public function locationCoordinates()
  {
      $location_coordinates = wmb_get_location_coordinates(wmb_get_current_branch_id());
      return $location_coordinates;
  }
}
