<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageAssociationsPartnerships extends Controller
{
    public function homeBuildersAssociations()
    {
        $associations_partnerships = get_field('associations_partnerships', wmb_get_current_branch_id());
        return $associations_partnerships['home_builders_associations'];
    }

    public function constructionAssociations()
    {
        $associations_partnerships = get_field('associations_partnerships', wmb_get_current_branch_id());
        return $associations_partnerships['construction_associations'];
    }

    public function contractorAssociations()
    {
        $associations_partnerships = get_field('associations_partnerships', wmb_get_current_branch_id());
        return $associations_partnerships['contractor_associations'];
    }

    public function valuedPartnerships()
    {
        $associations_partnerships = get_field('associations_partnerships', wmb_get_current_branch_id());
        return $associations_partnerships['valued_partnerships'];
    }
}
