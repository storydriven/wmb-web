<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
  public function homeIntro()
  {
    return get_field('home_page_welcome',wmb_get_current_branch_id());
  }

  public function latestCommunityUpdate()
  {
    $args = array(
      'numberposts' => 1,
      'post_type'   => 'community-update'
    );

    $posts_array = get_posts( $args );

    $article_array = array(
      'title' => $posts_array[0]->post_title,
      'excerpt' => get_the_excerpt( $posts_array[0]->ID ),
      'thumbnail_url' => get_the_post_thumbnail_url( $posts_array[0]->ID ),
      'archive_url' => esc_url( get_permalink( get_page_by_title( 'Community Updates' ) ) )
    );

    return $article_array;
  }
}
