<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageAbout extends Controller
{
    public function about()
    {
        return get_field('about_us_section');
    }

    public function team()
    {
        return get_field('about_us', wmb_get_current_branch_id());
    }
}
