<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageWbiHomeWarranty extends Controller
{
    public function servicesOffered()
    {
        return get_field('services_offered_section');
    }
}
