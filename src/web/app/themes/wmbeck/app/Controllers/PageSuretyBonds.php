<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageSuretyBonds extends Controller
{
    public function commonBonds()
    {
        return get_field('common_bonds_section');
    }
}
