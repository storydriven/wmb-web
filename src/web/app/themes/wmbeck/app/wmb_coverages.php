<?php

/*
* Prepare coverages data, which is passed
* to wmb-coverages-app.js as "coveragesData"
*/
function get_coverages_data() {
  $args = array(
    'post_type' => 'coverage',
    'numberposts' => -1
  );

  $coverages = get_posts($args);
  $coverages_data = array();

  foreach( $coverages as $coverage ) {
    $thumbnail_url = get_the_post_thumbnail_url($coverage->ID);
    $fallback_thumbnail_url = \App\asset_path('images/FPO_800x600.png');
    $thumbnail = ( $thumbnail_url ) ? $thumbnail_url : $fallback_thumbnail_url;
    $terms = get_the_terms($coverage->ID, 'coverage_type');
    $terms_array = array();

    if ( !empty($terms) ) {
      foreach ($terms as $term) {
        array_push($terms_array, $term->term_id);
      }
    }

    $coverage_array = array(
      'id' => $coverage->ID,
      'thumbnail' => $thumbnail,
      'name' => get_the_title($coverage->ID),
      'short_description' => get_field('short_description', $coverage->ID),
      'long_description' => get_field('long_description', $coverage->ID),
      'example_claim' => get_field('example_claim', $coverage->ID),
      'terms' => $terms_array,
      'category_string' => get_coverage_terms_string($coverage->ID, ' | '),
      'action' => get_field('action', $coverage->ID),
      'login_page' => get_field('login_page', $coverage->ID)
    );

    array_push( $coverages_data, $coverage_array );
  }

  return $coverages_data;
}

/*
* Prepare coverages taxonomy data, which is passed
* to wmb-coverages-app.js as "coveragesTaxonomyData"
*/
function get_coverages_taxonomy_data() {
  $terms = get_terms( array(
      'taxonomy' => 'coverage_type',
      'hide_empty' => true,
  ) );

  $coverages_taxonomy = array();

  // Add parent terms first
  foreach ($terms as $key => $term) {
    if ( $term->parent === 0 && $term->slug !== 'financial' && $term->slug !== 'surety-bonding' ) {
      $parent_term = array(
        'term_id' => $term->term_id,
        'name' => $term->name,
        'slug' => $term->slug,
        'child_terms' => array()
      );
      $coverages_taxonomy[] = $parent_term;
    }
  }

  // Add child terms into child_terms array of parent
  foreach ($terms as $key=>$term ) {
    if ( $term->parent !== 0 ) {
      $parent_term_id = null;

      foreach ($coverages_taxonomy as $key_2=>$sorted_term) {
        if ( $sorted_term['term_id'] == $term->parent )
          $parent_term_id = $key_2;
      }

      $child_term = array(
        'term_id' => $term->term_id,
        'name' => $term->name,
        'slug' => $term->slug,
        'count' => $term->count
      );

      if ( !is_null($parent_term_id) ) {
        $coverages_taxonomy[$parent_term_id]['child_terms'][] = $child_term;
      } else {
        $coverages_taxonomy[] = $child_term;
      }
    }
  }

  return $coverages_taxonomy;
}

// Returns an array of terms for a given coverage, sorted heirarchically
function get_sorted_coverage_terms($coverage_id) {
  $coverage_terms_array = ( get_the_terms( $coverage_id, 'coverage_type') );
  $coverage_terms_sorted_array = array();

  if ( $coverage_terms_array ) {
    foreach ($coverage_terms_array as $key=>$term ) {
      if ( $term->parent === 0 ) {
        $parent_term = $term;
        $parent_term->child_terms = array();
        $coverage_terms_sorted_array[] = $parent_term;
      }
    }

    foreach ($coverage_terms_array as $key=>$term ) {
      if ( $term->parent !== 0 ) {
        // Find parent term in sorted array
        $parent_term_id = null;

        foreach ($coverage_terms_sorted_array as $key_2=>$sorted_term) {
          if ( $sorted_term->term_id == $term->parent )
            $parent_term_id = $key_2;
        }

        if ( !is_null($parent_term_id) ) {
          $coverage_terms_sorted_array[$parent_term_id]->child_terms[] = $term;
        } else {
          $coverage_terms_sorted_array[] = $term;
        }
      }
    }
  }

  return $coverage_terms_sorted_array;
}

/*
* Returns a string of the coverage's categories,
* sorted heirarchically and separated by a specified separator string
*/
function get_coverage_terms_string($coverage_id, $separator) {
  $coverage_terms_sorted_array = get_sorted_coverage_terms($coverage_id);
  $coverage_terms_string = '';

  if ( $coverage_terms_sorted_array ) {

    foreach ($coverage_terms_sorted_array as $key=>$term) {
      // Remove "Insurance" from term names
      $term_name = ( $term->name == 'Commercial Insurance' ) ? 'Commercial' : $term->name;
      $term_name = ( $term_name == 'Personal Insurance' ) ? 'Personal' : $term_name;
      // Build coverage category string
      $coverage_terms_string .= $term_name;
      if ( !empty( $term->child_terms ) ) {
        $coverage_terms_string .= $separator;
        foreach( $term->child_terms as $child_key=>$child_term ) {
          $coverage_terms_string .= $child_term->name;
          if ( $child_key < count($term->child_terms) - 1 )
            $coverage_terms_string .= ', ';
        }
      }
      if ( $key < count($coverage_terms_sorted_array) - 1 )
        $coverage_terms_string .= ', ';
    }

  }

  return $coverage_terms_string;
}
