<?php

// Options Pages
//
if( function_exists('acf_add_options_page') ) {

  $footer_fields = acf_add_options_page(array(
  	'page_title' 	=> 'Footer Fields',
  	'menu_title'	=> 'Footer Fields',
  	'menu_slug' 	=> 'footer-fields',
    'icon_url'    => 'dashicons-tagcloud',
  ));

  $testimonials = acf_add_options_page(array(
  	'page_title' 	=> 'Testimonials',
  	'menu_title'	=> 'Testimonials',
  	'menu_slug' 	=> 'testimonials',
    'icon_url'    => 'dashicons-format-quote',
  ));

}
