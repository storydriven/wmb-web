<?php

/**
* wmb_get_user_branch()
* Determines user's WMB branch,
* and stores it in WP_Cache for other functions to use
*/
function wmb_get_user_branch() {
  // Check if there's a branch stored in a cookie
  if(isset($_COOKIE['wmb_user_branch'])) {
    // If a branch is found in a cookie, save it as a variable in WP_Cache
    wp_cache_set( 'wmb_user_branch', $_COOKIE['wmb_user_branch'] );

  } else {
    // If a branch is not found in a cookie, find nearest branch location based on user's GeoIP
    $closest_branch = wmb_find_closest_branch(wmb_user_geolocation());

    // set a cookie for 1 year
    setcookie('wmb_user_branch', $closest_branch, time()+31556926);

    // save closest branch as a variable in WP_Cache
    wp_cache_set( 'wmb_user_branch', $closest_branch );
  }
}
add_action( 'init', 'wmb_get_user_branch' );

/**
* wmb_user_geolocation()
* Gets the user's approximate location by their IP
*/
function wmb_user_geolocation() {
  $user_location = array();
  // IP address
  $userIP = $_SERVER['REMOTE_ADDR'];

  // My IP address, for testing
  // $userIP = '50.98.207.139';

  // Alberta IP address, for testing
  // $userIP = '68.148.24.255';

  // Vancouver IP address, for testing
  // $userIP = '199.175.213.12';

  // Kelowna IP address, for testing
  // $userIP = '207.102.139.97';

  // API end URL
  $apiURL = 'https://freegeoip.app/json/' . $userIP;

  // Create a new cURL resource with URL
  $ch = curl_init($apiURL);

  // Return response instead of outputting
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // Execute API request
  $apiResponse = curl_exec($ch);

  // Close cURL resource
  curl_close($ch);

  // Retrieve IP data from API response
  $ipData = json_decode($apiResponse, true);

  if(!empty($ipData)) {
      $latitude = $ipData['latitude'];
      $longitude = $ipData['longitude'];
      $time_zone = $ipData['time_zone'];
      $user_location = [$latitude, $longitude, $time_zone];

      if ( $user_location !== [0,0] )
        return $user_location;
  }

  return false;
}

// Determines closest WMB branch based on user's coordinates
function wmb_find_closest_branch($user_coordinates) {
  $closest_branch = '';

  if ( $user_coordinates ) {
    $location_distances = array();
    $wmb_branch_coordinates = wmb_branch_coordinates();

    /*
    * For each branch in $wmb_branch_coordinates,
    * do some math to get the distance between user's
    * location and branch location, then push that
    * distance value into $location_distances array
    */
    foreach( $wmb_branch_coordinates as $key => $branch ) {
      $branch_name = $wmb_branch_coordinates[$key]['name'];
      $lat_diff = abs($branch['coordinates'][0] - $user_coordinates[0]);
      $lng_diff = abs($branch['coordinates'][1] - $user_coordinates[1]);
      $distance_index = $lat_diff + $lng_diff;

      array_push( $location_distances, [ $key, $branch_name, $distance_index ] );
    }

    // Sort array of location distances from closest to farthest
    function cmp($a, $b) {
      if ( $a[2] == $b[2] ) return 0;

      return ( $a[2] < $b[2] ) ? -1 : 1;
    }
    usort( $location_distances, 'cmp' );

    $closest_branch = $location_distances[0][1];

  } else {
    // If user coordinates are not available, default to a particular branch
    $closest_branch = 'burnaby';
  }

  return $closest_branch;
}

function wmb_branch_coordinates() {
  return array(
    [
      'name' => 'victoria',
      'coordinates' => [ 48.4262607, -123.394242 ]
    ],
    [
      'name' => 'kelowna',
      'coordinates' => [ 49.8875565, -119.4955444 ]
    ],
    [
      'name' => 'kamloops',
      'coordinates' => [ 50.6745, -120.3273 ]
    ],
    [
      'name' => 'fraser-valley',
      'coordinates' => [ 49.1042, -122.6604 ]
    ],
    [
      'name' => 'burnaby',
      'coordinates' => [ 49.2488, -122.9805 ]
    ],
    [
      'name' => 'calgary',
      'coordinates' => [ 51.0447, -114.0719 ]
    ],
    [
      'name' => 'edmonton',
      'coordinates' => [ 53.5461, -113.4938 ]
    ],
    [
      'name' => 'prince-george',
      'coordinates' => [ 53.9171, -122.7497 ]
    ]
  );
}

// Returns post ID of location, based on slug stored in wmb_user_branch
function wmb_get_current_branch_id() {
  $user_branch = wp_cache_get( 'wmb_user_branch' );
  $branch_location_post = get_posts([
    'name' => $user_branch,
    'post_type' => 'location'
  ])[0];

  return $branch_location_post->ID;
}

/*
* Determines whether a given branch is currently open
* Returns an array with is_open (boolean) and next_open_close (string)
*/
function wmb_branch_is_open($branch_id) {
  $location_info = get_field('location_info',$branch_id);
  $branch_time_zone_abbrev = $location_info['hours']['time_zone'];
  $branch_time_zone =  new DateTimeZone(timezone_name_from_abbr($branch_time_zone_abbrev));
  $branch_current_datetime = new DateTime('now', $branch_time_zone);
  $today_key = null;

  $branch_daily_hours = $location_info['hours']['hours_daily'];
  $branch_todays_hours = array();
  $branch_open_close_info = array(
    'is_open' => false,
    'next_open_close' => false
  );

  // Find today's hours for the location
  foreach ($branch_daily_hours as $key=>$day) {
    if ( $day['day'] == strtolower($branch_current_datetime->format('l')) ) {
      $branch_todays_hours['open'] = $day['open'];
      $branch_todays_hours['close'] = $day['close'];
      $today_key = $key;
    }
  }

  if ( !empty($branch_daily_hours) ) {
    // If hours exist for today
    if ( !empty($branch_todays_hours) ) {
      $open_time = new DateTime($branch_todays_hours['open'], $branch_time_zone);
      $close_time = new DateTime($branch_todays_hours['close'], $branch_time_zone);

      // If branch is open right now
      if ( $branch_current_datetime >= $open_time && $branch_current_datetime < $close_time ) {
        $branch_open_close_info['is_open'] = true;
        $branch_open_close_info['next_open_close'] = 'Closes ' . wmb_get_formatted_time($close_time);
      } else {
        // Get next opening time
        $next_day_open = array();

        if ( !is_null($today_key) ) {
          $i = ( $today_key + 1 < count($branch_daily_hours) ) ? $today_key + 1 : 0;

          do {
            if ( !empty($branch_daily_hours[$i]) && !empty($branch_daily_hours[$i]['open']) ) {
              $next_day_open = array(
                'day' => $branch_daily_hours[$i]['day'],
                'time' => $branch_daily_hours[$i]['open']
              );
            }

            $i = ($i + 1) % count($branch_daily_hours);
          } while ( $i != $today_key && empty($next_day_open) );

          if ( !empty($next_day_open) ) {
            $next_open_day = new DateTime($next_day_open['day'], $branch_time_zone);
            $next_open_time = new DateTime($next_day_open['time'], $branch_time_zone);
            $branch_open_close_info['next_open_close'] = 'Opens ' . $next_open_day->format('D') . ' ' . wmb_get_formatted_time($next_open_time);
          }
        }

      }
    }
  }

  return $branch_open_close_info;

}

// If time is on the hour, format is '5 pm'. Otherwise if minutes are needed, format is '5:30 pm'.
function wmb_get_formatted_time($date_time) {
  if ( $date_time->format('i') !== '00' ) {
    return $date_time->format('g:i a');
  } else {
    return $date_time->format('g a');
  }
}

function wmb_get_branch_list() {
  $args = array(
    'post_type' => 'location',
    'numberposts' => -1
  );

  $branches = get_posts($args);
  $branch_list = array();

  foreach ($branches as $branch) {
    $name = get_the_title($branch->ID);
    $slug = get_post_field('post_name', $branch->ID);

    $branch_array = array(
      'name'              => $name,
      'slug'              => $slug,
      'selected'          => ( $branch->ID === wmb_get_current_branch_id() )
    );
    array_push( $branch_list, $branch_array );
  }

  return $branch_list;
}

// Gets a branch's geo coordinates from Google's geocoding API
function wmb_get_location_coordinates($location_id) {
  $location_info_data = get_field('location_info', $location_id );
  // API end URL
  $apiURL = 'https://maps.googleapis.com/maps/api/geocode/json';
  $address = str_replace('#','',$location_info_data['address']);
  $address = urlencode( strip_tags( $address ) );
  $api_key = 'AIzaSyBxj6gFdeki0Jgpxb3cAdsYnsco7Ze8MlQ';
  $api_request = $apiURL . '?address=' . $address . '&key=' . $api_key;

  // Create a new cURL resource with URL
  $ch = curl_init($api_request);

  // Return response instead of outputting
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // Execute API request
  $apiResponse = curl_exec($ch);

  // Close cURL resource
  curl_close($ch);

  // Retrieve IP data from API response
  $apiResponse = json_decode($apiResponse, true);

  if(!empty($apiResponse)) {
    return $apiResponse['results'][0]['geometry']['location'];
  }

  return false;
}
