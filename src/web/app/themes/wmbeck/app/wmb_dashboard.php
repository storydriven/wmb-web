<?php

// Add Coverage Type column, and remove Date column
add_filter('manage_coverage_posts_columns', function( $columns ) {
    $columns['coverage_type']  = 'Coverage Type';
    unset($columns['date']);
    return $columns;
});

// Echo coverage_type taxonomy terms inside Coverage Type column
add_action( 'manage_coverage_posts_custom_column', function( $column_name, $post_id ) {
    // "Coverage Type" column
    if ($column_name == 'coverage_type') {
      echo get_coverage_terms_string($post_id, ' -> ');
    }
}, 10, 2 );

// Make Coverage Type column sortable
add_filter( 'manage_edit-coverage_sortable_columns', function( $columns ) {
  $columns['coverage_type'] = 'coverage_type';
  return $columns;
} );

add_filter( 'request', function( $vars ) {
  if ( isset( $vars['orderby'] ) && 'coverage_type' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
      'orderby' => 'taxonomy, name'
    ) );
  }
  return $vars;
} );

// Change 'Posts' to 'Blog' in WP dashboard
add_action( 'admin_menu', 'wmb_change_post_menu_label' );
function wmb_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    echo '';
}
