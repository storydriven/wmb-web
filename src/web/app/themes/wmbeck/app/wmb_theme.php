<?php

namespace App;

// Login Page Customizations
add_action( 'login_enqueue_scripts', function() { ?>
  <style type="text/css">
    body.login {
      background: #003B5C;
    }
    #login h1 a, .login h1 a {
      background-image: url(<?php echo asset_path('images/wmb-logo.svg'); ?>);
  		width:264px;
      height:110px;
  		background-size: 264px 110px;
  		background-repeat: no-repeat;
    	padding-bottom: 0px;
      margin-bottom: 40px;
    }
    .login #nav a,
    .login #backtoblog a,
    .privacy-policy-page-link a {
      color: white !important;
    }
    .login #nav a:hover,
    .login #backtoblog a:hover,
    .privacy-policy-page-link a:hover {
      color: #FF6908 !important;
    }
  </style>
<?php } );

add_filter( 'login_headerurl', function() {
    return home_url();
} );

add_filter( 'login_headertext', function() {
  return 'Wilson M. Beck';
} );

// Customize the excerpt "Read More" link
add_filter('excerpt_more', function($more) {
  global $post;
	return ' <a class="moretag" href="'. get_permalink($post->ID) . '">Read more ></a>';
});
