@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Intro --}}
    @include('partials.global.intro')
    {{-- Articles List --}}
    @include('partials.community-updates.articles-list')
  @endwhile
@endsection
