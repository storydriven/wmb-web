@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Hero --}}
    @include('partials.global.hero')
    {{-- Intro --}}
    @include('partials.global.intro')
    {{-- About --}}
    @include('partials.about.about')
    {{-- Team --}}
    @include('partials.about.team')
    {{-- Testimonials --}}
    @include('partials.global.testimonials')
    {{-- CTA --}}
    @include('partials.about.cta')
  @endwhile
@endsection
