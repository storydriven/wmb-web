@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Intro --}}
    @include('partials.associations-partnerships.intro')
    {{-- Intro --}}
    @include('partials.associations-partnerships.associations-partnerships')
    {{-- CTA --}}
    @include('partials.global.cta')
  @endwhile
@endsection
