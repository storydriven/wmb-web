@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Popular Coverages --}}
    @include('partials.global.popular-coverages')
    {{-- Coverages --}}
    @include('partials.insurance.coverages')
    {{-- CTA --}}
    @include('partials.global.cta')
  @endwhile
@endsection
