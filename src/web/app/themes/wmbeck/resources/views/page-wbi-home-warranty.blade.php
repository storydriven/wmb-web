@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Hero --}}
    @include('partials.global.hero')
    {{-- Intro --}}
    @include('partials.global.intro')
    {{-- Services Offered --}}
    @include('partials.wbi-home-warranty.services-offered')
    {{-- CTA --}}
    @include('partials.global.cta')
  @endwhile
@endsection
