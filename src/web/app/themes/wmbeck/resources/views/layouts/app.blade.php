<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    {{-- Header --}}
    @php do_action('get_header') @endphp
    @include('partials.header')
    {{-- Content --}}
    <div class="content" role="document" id="app">
      <main class="main">
        @yield('content')
      </main>
    </div>
    {{-- Modals --}}
    @include('partials.modals.change-location')
    @include('partials.modals.client-login')
    {{-- Footer --}}
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
    {{-- Footer Scripts --}}
    <script>
      @stack('footer_scripts')
      (function($) {
        $(document).ready(function(){
          @stack('footer_scripts_document_ready')
        });
      })( jQuery );
    </script>
  </body>
</html>
