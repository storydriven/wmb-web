@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    <div class="container">
      <h1>{!! _e('Off Duty Program','sage') !!}</h1>
      <h2>{!! _e('Log In','sage') !!}</h2>
      <div class="form-container">
        <form name="portalSignon" id="offDutyProgramLoginForm" method="post" action="https://portal.csr24.ca/mvc/2100715904">
  				<input type="hidden" name="AgencyKey" value="2100715904" />
          <div class="form-group">
            <label for="offDutyProgramLoginUsername">{!! _e('Username*','sage') !!}</label>
            <input type="text" class="form-control" id="offDutyProgramLoginUsername" name="Username" size="99" required />
          </div>
  				<div class="form-group">
            <label for="offDutyProgramLoginPassword">{!! _e('Password*','sage') !!}</label>
            <input type="password" class="form-control" id="offDutyProgramLoginPassword" name="Password" size="18" required />
          </div>
  				<div class="form-check">
            <input type="checkbox" class="form-check-input" id="offDutyProgramLoginRememberMe" name="RememberMe" value="Remember"/>
            <label class="form-check-label" for="offDutyProgramLoginRememberMe">{!! _e('Remember me','sage') !!}</label>
          </div>
  				<input type="button" class="btn btn-default" name="signon" value="{!! _e('Login','sage') !!}" id="offDutyProgramLoginFormSubmit" />
          <div class="register-forgot-links">
            <a href="https://portal.csr24.ca/mvc/Account/SignUp?AgencyKey=2100715904" target="new">Register Account</a>
    				<a href="https://portal.csr24.ca/mvc/Account/ForgotPassword/2100715904" target="new">Forgot Details?</a>
          </div>
  			</form>
      </div>
    </div>
  @endwhile
@endsection

@push('footer_scripts')
  const offDutyProgramLoginFormSubmit = document.getElementById("offDutyProgramLoginFormSubmit");
  offDutyProgramLoginFormSubmit.addEventListener("click", offDutyProgramLogin, false);

  function offDutyProgramLogin() {
    const offDutyProgramLoginForm = document.getElementById('offDutyProgramLoginForm');

    if (offDutyProgramLoginForm.Username.value > '' && offDutyProgramLoginForm.Password.value > '') {
      if (offDutyProgramLoginForm.RememberMe.checked == true) {
        {{-- setCookie('Username', offDutyProgramLoginForm.Username.value); --}}
      } else {
        {{-- setCookie('Username', ''); --}}
      }
      offDutyProgramLoginForm.submit();
    } else {
      alert('Please enter a username and password');
      offDutyProgramLoginForm.Username.focus();
    }
  }
@endpush
