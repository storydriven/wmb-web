@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Hero --}}
    @include('partials.global.hero')
    {{-- Intro --}}
    @include('partials.contact.intro')
    {{-- About --}}
    @include('partials.contact.contact')
  @endwhile
@endsection
