@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Hero --}}
    @include('partials.global.hero')
    {{-- Intro --}}
    @include('partials.global.intro')
    {{-- Common Bonds --}}
    @include('partials.surety-bonds.common-bonds')
    {{-- CTA --}}
    @include('partials.global.cta')
  @endwhile
@endsection
