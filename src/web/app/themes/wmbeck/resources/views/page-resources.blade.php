@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Intro --}}
    @include('partials.global.intro')
  @endwhile
@endsection
