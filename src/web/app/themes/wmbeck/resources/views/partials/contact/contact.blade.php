<section class="contact-main container">
  <div class="contact-outer">
    {{-- Contact Form --}}
    <div class="contact-form-container">
      <h2>{!! _e('Contact us using the form below','sage') !!}</h2>
      {!! do_shortcode('[contact-form-7 id="385" title="Contact Form"]') !!}
    </div>
    {{-- Contact Info --}}
    <div class="contact-info-container">
      @if ( !empty( $location_coordinates ) )
        <div class="location-map">
          <div class="map-container">
            <div id="map"></div>
            <div class="canvas-size-element"></div>
          </div>
        </div>
      @endif
      <div class="location">
        <h3>{!! _e('Location','sage') !!}</h3>
        <div class="contact-section-copy">
          {!! $location_info['address'] !!}
        </div>
        <a class="directions-link" href="{!! $location_info['directions_uri'] !!}">
          {!! _e('Get directions >','sage') !!}
        </a>
      </div>
      <div class="hours">
        <h3>{!! _e('Hours','sage') !!}</h3>
        <div class="contact-section-copy">
          {!! $location_info['hours'] !!}
        </div>
      </div>
      <div class="contact">
        <h3>{!! _e('Contact','sage') !!}</h3>
        <div class="contact-section-copy">
          <span class="contact-number-label">PH</span> {!! $location_info['phone'] !!}<br />
          <span class="contact-number-label">TF</span> {!! $location_info['toll_free'] !!}<br />
          <span class="contact-number-label">FX</span> {!! $location_info['fax'] !!}<br />
        </div>
        <a class="email-link" href="mailto:{!! $location_info['email'] !!}">
          {!! $location_info['email'] !!}
        </a>
      </div>
    </div>
  </div>
</section>

@if ( !empty( $location_coordinates ) )
  @push('footer_scripts')
    function initMap() {
      console.log('initMap()')
      let mapDiv = document.getElementById('map'),
          theLocation = {
            lat: {!! $location_coordinates['lat'] !!},
            lng: {!! $location_coordinates['lng'] !!}
          },
          options = {
            center: theLocation,
            zoom: 11,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: true
          },
          map = new google.maps.Map(mapDiv, options),
          marker = new google.maps.Marker({
            position: theLocation,
            map: map,
          });
    }
  @endpush

  @push('footer_scripts_document_ready')
    initMap();
  @endpush
@endif

@push('footer_scripts_document_ready')
  {{-- Populates the "Your WMB Branch" field --}}
  const wmbBranchSelect = document.querySelector('form #yourBranch');

  for (var i in locationData) {
    let branch = locationData[i],
        branchOption = document.createElement('option');

    branchOption.value = branch.name;
    branchOption.innerHTML = branch.name;
    branchOption.selected = branch.selected;

    wmbBranchSelect.appendChild(branchOption);
  };
@endpush
