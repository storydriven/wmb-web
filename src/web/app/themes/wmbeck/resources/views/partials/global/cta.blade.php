<section class="cta-section">
  <div class="container cta-container">
    <h1>{{ $call_to_action['headline'] }}</h1>
    <div class="copy">
      {!! $call_to_action['copy'] !!}
    </div>
    @if ($call_to_action['buttons'])
      <div class="buttons">
        @foreach ($call_to_action['buttons'] as $button)
          <a href="{!! $button['button']['url'] !!}" class="btn btn-default" @if ($button['button']['target'])target="{{ $button['button']['target'] }}"@endif>
            {{ $button['button']['title'] }}
          </a>
        @endforeach
      </div>
    @endif
  </div>
</section>
