<section class="intro">
  <div class="container">
    <h1>{{ $home_intro['headline'] }}</h1>
    <div class="intro-copy">
      {!! $home_intro['copy'] !!}
    </div>
    @if ($home_intro['buttons'])
      <ul class="buttons">
        @foreach ($home_intro['buttons'] as $button)
          <li>
            <a href="{{ $button['button']['url'] }}" class="btn btn-default">
              {{ $button['button']['title'] }}
            </a>
          </li>
        @endforeach
      </ul>
    @endif
  </div>
</section>
