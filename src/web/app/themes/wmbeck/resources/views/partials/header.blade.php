<header class="banner">
  <a class="brand d-none d-lg-block" href="{{ home_url('/') }}">
    <img class="logo-horizontal" src="@asset('images/wmb-logo.svg')" alt="{!! _e('Wilson M. Beck Insurance Services','sage') !!}">
    <img class="logo-small" src="@asset('images/wmb-logo-small.svg')" alt="{!! _e('Wilson M. Beck Insurance Services','sage') !!}">
  </a>
  {{-- Secondary Nav Container (Top bar) --}}
  <div class="container-fluid secondary-nav-container">
    {{-- My WMB Info & Dropdown --}}
    <div class="dropdown my-wmb">
      <button class="my-wmb-dropdown-button" type="button" id="myWmbDropdownButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <div class="my-wmb-info">
          <i class="fas fa-map-marker-alt"></i>
          <b>{!! _e('My WMB:','sage') !!}</b>
          {{ $location_info['name'] }}
          <span class="my-wmb-divider">|</span>
        </div>
        <div class="open-close-info">
          <span class="open-closed {{ $location_info['is_open'] ? 'open' : 'closed' }}">
            {{ $location_info['is_open'] ? 'Open' : 'Closed' }}:
          </span>
          <span class="next-open-close">
            {{ $location_info['next_open_close'] }}
          </span>
        </div>
        <i class="fas fa-chevron-down"></i>
      </button>
      {{-- My WMB Dropdown Menu --}}
      @include('partials.global.my-wmb-dropdown')
    </div>
    {{-- Search field (desktop) --}}
    <div class="navbar-search-container d-none d-lg-flex">
      <form class="form-inline">
        <div class="input-group">
          <input class="form-control" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="search-button input-group-text" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
    {{-- Secondary Nav --}}
    <nav class="nav-secondary d-none d-lg-flex">
      @if (has_nav_menu('secondary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']) !!}
      @endif
      <div class="nav user-login">
        <div class="menu-item">
          <a href="#clientLoginModal" data-toggle="modal" data-target="#clientLoginModal">
            <i class="fas fa-user"></i>
            {{ _e('Client Login','sage') }}
          </a>
        </div>
      </div>
    </nav>
  </div>
  {{-- Primary Nav Container --}}
  <div class="container primary-nav-container">
    <a class="brand-mobile d-lg-none" href="{{ home_url('/') }}">
      <img class="logo-small" src="@asset('images/wmb-logo-small.svg')" alt="{!! _e('Wilson M. Beck Insurance Services','sage') !!}">
    </a>
    {{-- Search field (mobile) --}}
    <div class="navbar-search-container-mobile d-flex d-lg-none">
      <form class="form-inline">
        <div class="input-group">
          <input class="form-control" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="search-button input-group-text" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
    {{-- Nav toggle (mobile) --}}
    <button type="button" name="openMobileNav" class="nav-open-button d-lg-none">
      <img class="nav-open-icon" src="@asset('images/nav-open-icon.svg')">
    </button>
    {{-- Primary Navigation --}}
    <div class="primary-nav-panel">
      <button type="button" name="closeMobileNav" class="nav-close-button d-lg-none">
        <i class="far fa-times-circle"></i>
      </button>
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
      <nav class="nav-primary-right">
        @if (has_nav_menu('primary_navigation_right'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation_right', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
      {{-- Secondary Nav Items (Mobile Only) --}}
      <nav class="nav-secondary d-flex d-lg-none">
        @if (has_nav_menu('secondary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']) !!}
        @endif
        <div class="nav user-login">
          <div class="menu-item">
            <a href="#clientLoginModal" data-toggle="modal" data-target="#clientLoginModal">
              <i class="fas fa-user"></i>
              {{ _e('Client Login','sage') }}
            </a>
          </div>
        </div>
      </nav>
      {{-- Social Links (Mobile Only) --}}
      <ul class="social-links d-flex d-lg-none">
        @foreach ($social_links as $social_link)
          <li>
            <a href="{{ $social_link['url'] }}">
              <i class="fab {{ $social_link['icon_class'] }}"></i>
            </a>
          </li>
        @endforeach
      </ul>

      <div class="hr-outer d-lg-none">
        <hr>
      </div>
    </div>
  </div>
</header>

@push('footer_scripts_document_ready')
  $('.nav-open-button').on('click', function() {
    if ( !$('.primary-nav-panel').hasClass('show') ) {
      $('.primary-nav-panel').addClass('show')
    }
  });

  $('.nav-close-button').on('click', function() {
    if ( $('.primary-nav-panel').hasClass('show') ) {
      $('.primary-nav-panel').removeClass('show')
    }
  });
@endpush
