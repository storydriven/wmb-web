<section class="about container">
  <div class="about-inner">
    <div class="image-container">
      <img src="{{ $about['image'] }}">
    </div>
    <div class="text-container">
      <h2>{{ $about['headline'] }}</h2>
      {!! $about['copy'] !!}
    </div>
  </div>
</section>
