@if ($latest_community_update)
  <section class="community-updates">
    <div class="container">
      <h1>{!! _e('Community Updates','sage') !!}</h1>
      <article class="latest-community-update">
        <img class="article-thumbnail img-fluid" src="{!! $latest_community_update['thumbnail_url'] !!}" alt="{{ $latest_community_update['title'] }}">
        <h2>{{ $latest_community_update['title'] }}</h2>
        <div class="article-excerpt">
          {!! $latest_community_update['excerpt'] !!}
        </div>
      </article>
      <a href="{!! $latest_community_update['archive_url'] !!}" class="btn btn-default view-all-updates">
        {!! _e('View All Updates','sage') !!}
      </a>
    </div>
  </section>
@endif
