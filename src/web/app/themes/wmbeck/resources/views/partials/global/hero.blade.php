<section class="{{ array_key_exists('logo_image', $hero) ? 'hero screen' : 'hero' }}">
  <div class="image-size-element"></div>
  @if (array_key_exists('logo_image', $hero))
    <div class="logo-image">
      <img src="{!! $hero['logo_image'] !!}">
    </div>
  @endif
</section>

@push('styles')
  section.hero {
    background-image: url('{{ $hero['image_mobile'] }}');
  }
  @media(min-width: {{ $breakpoints['lg'] . 'px' }}) {
    section.hero {
      background-image: url('{{ $hero['image_desktop'] }}');
    }
  }
@endpush
