<section class="testimonials">
  <div class="container">
    <h1>{!! _e('What Our Clients Say','sage') !!}</h1>
    <div class="slider-outer">
      @component('components.slider-arrow-left')@endcomponent
      <div class="testimonials-slider">
        @foreach ($testimonials as $key => $testimonial)
          <div class="testimonial">
            @if ($testimonial['logo'])
              <img src="{!! $testimonial['logo'] !!}" alt="{{ $testimonial['company_name'] }}">
              <p>{{ $testimonial['quote'] }}</p>
              <div class="attribution">
                {{ $testimonial['attribution'] }}
              </div>
            @endif
          </div>
        @endforeach
      </div>
      @component('components.slider-arrow-right')@endcomponent
    </div>
    <hr>
  </div>
</section>

@push('footer_scripts_document_ready')
  $('.testimonials-slider').slick({
    arrows: true,
    dots: true,
    prevArrow: $('.testimonials .wmb-slider-controls.prev .wmb-slider-button'),
    nextArrow: $('.testimonials .wmb-slider-controls.next .wmb-slider-button'),
  });
@endpush
