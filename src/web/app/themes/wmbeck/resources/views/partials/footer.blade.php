<footer class="content-info">
  <div class="footer-upper">
    <div class="container footer-columns">
      {{-- Coverage --}}
      <div class="footer-column">
        <div class="column-title">
          {!! _e('Coverage','sage') !!}
        </div>
        <div class="nav-footer-coverage">
          @if (has_nav_menu('footer_coverage'))
            {!! wp_nav_menu(['theme_location' => 'footer_coverage', 'menu_class' => 'nav']) !!}
          @endif
        </div>
      </div>
      {{-- Quick Links --}}
      <div class="footer-column">
        <div class="column-title">
          {!! _e('Quick Links','sage') !!}
        </div>
        <div class="nav-footer-quick-links">
          @if (has_nav_menu('footer_quick_links'))
            {!! wp_nav_menu(['theme_location' => 'footer_quick_links', 'menu_class' => 'nav']) !!}
          @endif
        </div>
      </div>
      {{-- Contact --}}
      <div class="footer-column">
        <div class="column-title">
          {!! _e('Contact','sage') !!}
        </div>
        <div class="address">
          WMB {{ $location_info['name'] }}<br />
          {!! $location_info['address'] !!}
        </div>
        <div class="contact-info">
          <span>{!! _e('PH','sage') !!}</span> {{ $location_info['phone'] }}<br />
          @if ($location_info['toll_free'])
            <span>{!! _e('TF','sage') !!}</span> {{ $location_info['toll_free'] }}<br />
          @endif
          @if ($location_info['fax'])
            <span>{!! _e('FX','sage') !!}</span> {{ $location_info['fax'] }}<br />
          @endif
          <a href="mailto:{{ $location_info['email'] }}" class="email-address">
            {{ $location_info['email'] }}
          </a>
        </div>
      </div>
      {{-- Follow WMB --}}
      <div class="footer-column">
        <div class="column-title">
          {!! _e('Follow WMB','sage') !!}
        </div>
        <ul class="social-links">
          @foreach ($social_links as $social_link)
            <li>
              <a href="{{ $social_link['url'] }}">
                <i class="fab {{ $social_link['icon_class'] }}"></i>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
      {{-- Partner Companies --}}
      <div class="footer-column">
        <div class="column-title">
          {!! _e('Partner Companies','sage') !!}
        </div>
        <ul class="partner-companies">
          @foreach ($footer_partner_companies as $company)
            <li>
              @if ($company['url'])
                <a href="{{ $company['url'] }}" target="_blank">
                  <img src="{{ $company['logo']['url'] }}" alt="{{ $company['name'] }}" width="{{ $company['logo']['width'] / 2 }}">
                </a>
              @else
                <img src="{{ $company['logo']['url'] }}" alt="{{ $company['name'] }}" width="{{ $company['logo']['width'] / 2 }}">
              @endif
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-lower">
    <div class="container">
      &copy; {{ $this_year }} {!! _e('Wilson M. Beck Insurance Group', 'sage') !!}
      <span class="divider">|</span><a href="{!! esc_url( get_permalink( get_page_by_title( 'Privacy Policy' ) ) ) !!}">{!! _e('Privacy Policy','sage') !!}</a>
      <span class="divider">|</span><a href="{!! esc_url( get_permalink( get_page_by_title( 'Terms of Use' ) ) ) !!}">{!! _e('Terms of Use','sage') !!}</a>
    </div>
  </div>
</footer>
