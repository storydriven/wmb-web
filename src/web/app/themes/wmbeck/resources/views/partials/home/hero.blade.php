<section class="hero home">
  <div class="hero-content">
    {!! $hero['headline'] !!}
  </div>
  <div class="slider-outer">
    <div class="home-hero-slider">
      @foreach ($hero['images'] as $key => $hero_image)
        <div class="hero-image image-{{ $key }}">
          <div class="image-size-element"></div>
        </div>
      @endforeach
    </div>
  </div>
</section>

@push('styles')
  @foreach ($hero['images'] as $key => $hero_image)
    section.hero .slider-outer .home-hero-slider .image-{{ $key }} {
      background-image: url('{{ $hero_image['image_mobile'] }}');
    }
    @media(min-width: {{ $breakpoints['lg'] . 'px' }}) {
      section.hero .slider-outer .home-hero-slider .image-{{ $key }} {
        background-image: url('{{ $hero_image['image_desktop'] }}');
      }
    }
  @endforeach
@endpush

@push('footer_scripts_document_ready')
  $('.home-hero-slider').slick({
    arrows: false,
    autoplay: true,
    dots: true
  });
@endpush
