<div class="modal client-login fade" id="clientLoginModal" tabindex="-1" role="dialog" aria-labelledby="clientLoginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="clientLoginModalLabel">
          {!! _e('WMB Client Login','sage') !!}
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="far fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
        <form name="portalSignon" id="clientLoginForm" method="post" action="https://portal.csr24.ca/mvc/2100715904">
        	<input type="hidden" name="AgencyKey" value="2100715904">
        	<div class="form-group">
            <label for="clientLoginUsername">{!! _e('Username*','sage') !!}</label>
            <input class="form-control" type="text" name="Username" size="99" id="clientLoginUsername" required>
          </div>
          <div class="form-group">
            <label for="clientLoginPassword">{!! _e('Password*','sage') !!}</label>
            <input class="form-control" type="password" name="Password" size="18" id="clientLoginPassword" required>
          </div>
          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="clientLoginRememberMe" name="RememberMe" value="Remember">
            <label class="form-check-label" for="clientLoginRememberMe">{!! _e('Remember me','sage') !!}</label>
          </div>
        	<input type="button" name="signon" value="{!! _e('Login','sage') !!}" class="btn btn-default" id="clientLoginFormSubmit">
        	<div class="register-forgot-links">
            <a href="https://portal.csr24.ca/mvc/Account/ForgotPassword/2100715904" target="new">{!! _e('Forgot Password?','sage') !!}</a>
            <a href="https://portal.csr24.ca/mvc/Account/SignUp?AgencyKey=2100715904" target="new">{!! _e('Create an Account','sage') !!}</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@push('footer_scripts')
  const clientLoginFormSubmit = document.getElementById("clientLoginFormSubmit");
  clientLoginFormSubmit.addEventListener("click", clientLogin, false);

  function clientLogin() {
    const clientLoginForm = document.getElementById('clientLoginForm');

    if (clientLoginForm.Username.value > '' && clientLoginForm.Password.value > '') {
      if (clientLoginForm.RememberMe.checked == true) {
        {{-- setCookie('Username', clientLoginForm.Username.value); --}}
      } else {
        {{-- setCookie('Username', ''); --}}
      }
      clientLoginForm.submit();
    } else {
      alert('Please enter a username and password');
      clientLoginForm.Username.focus();
    }
  }
@endpush
