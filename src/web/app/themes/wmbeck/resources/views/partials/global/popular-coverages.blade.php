@if ($location_popular_coverages)
  <section class="popular-coverages">
    <div class="container">
      <h1>Popular Coverages in {{ $location_info['name'] }}</h1>
      <div class="slider-outer">
        @component('components.slider-arrow-left')@endcomponent
        <div class="popular-coverages-slider">
          @foreach ($location_popular_coverages as $key => $coverage)
            <div class="coverage-item">
              <div class="thumbnail-image" style="background-image:url('{{ $coverage['thumbnail'] }}')">
                <div class="image-size-element"></div>
              </div>
              <h4 class="coverage-category">{!! $coverage['category'] !!}</h4>
              <h3 class="coverage-name">{!! $coverage['name'] !!}</h3>
              <p>{{ $coverage['short_description'] }}</p>
              <a class="btn btn-default more-button" href="#requestQuoteModal" data-toggle="modal" data-target="#requestQuoteModal">
                {!! _e('Request Quote','sage') !!}
              </a>
            </div>
          @endforeach
        </div>
        @component('components.slider-arrow-right')@endcomponent
      </div>
      @if (is_page('home'))
        <a class="btn btn-primary" href="{!! esc_url( get_permalink( get_page_by_title( 'Insurance' ) ) ) !!}">{!! _e('Explore Insurance','sage') !!}</a>
      @endif
    </div>
  </section>
@endif

@push('footer_scripts_document_ready')
  $('.popular-coverages-slider').slick({
    dots: true,
    mobileFirst: true,
    respondTo: 'slider',
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.popular-coverages .wmb-slider-controls.prev .wmb-slider-button'),
    nextArrow: $('.popular-coverages .wmb-slider-controls.next .wmb-slider-button'),
    responsive: [
      {
        breakpoint: 953,
        settings: {
          arrows: true,
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 689,
        settings: {
          arrows: true,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 575,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
@endpush
