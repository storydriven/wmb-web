<section class="intro">
  <div class="container">
    <h1>{{ $intro['headline'] }}</h1>
    <div class="intro-copy">
      {!! $intro['copy'] !!}
    </div>
    @if ($intro['buttons'])
      <ul class="buttons">
        @foreach ($intro['buttons'] as $button)
          <li>
            <a href="{{ $button['button']['url'] }}" class="btn btn-default">
              {{ $button['button']['title'] }}
            </a>
          </li>
        @endforeach
      </ul>
    @endif
  </div>
</section>
