<div class="modal change-location fade" id="changeLocationModal" tabindex="-1" role="dialog" aria-labelledby="changeLocationModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title" id="changeLocationModalLabel">
          {!! _e('Select your default WMB Office','sage') !!}
        </h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="far fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
        <div class="change-location-list">
          @foreach ($change_location_modal_options as $location)
            <div class="location-option">
              <div class="location-info">
                <div class="location-name">{{ $location['name'] }}</div>
                <p>
                  {{ $location['address'] }}<br />
                  {{ $location['hours'] }}<br />
                  {{ $location['phone'] }}
                </p>
              </div>
              <div class="button-container">
                <button
                  type="button"
                  name="button"
                  class="btn btn-default change-location-button"
                  @if ($location['selected'])
                    class="btn btn-default"
                    disabled
                  @else
                    class="btn btn-default change-location-button"
                    data-location="{{ $location['slug'] }}"
                  @endif
                >
                  @if ($location['selected'])
                    Selected
                  @else
                    Select
                  @endif
                </button>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>

@push('footer_scripts_document_ready')
@endpush
