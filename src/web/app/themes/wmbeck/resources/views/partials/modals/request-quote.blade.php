<div class="modal request-quote fade" id="requestQuoteModal" tabindex="-1" role="dialog" aria-labelledby="requestQuoteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="requestQuoteModalLabel">
          {!! _e('Modal Title','sage') !!}
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        [Request Quote Modal TBA]
      </div>
    </div>
  </div>
</div>

@push('footer_scripts_document_ready')
@endpush
