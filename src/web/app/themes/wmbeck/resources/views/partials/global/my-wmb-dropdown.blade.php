<div class="dropdown-menu my-wmb" aria-labelledby="myWmbDropdownButton">
  <div class="dropdown-header">
    <h4>
      {!! sprintf( __( 'WMB %s', 'sage' ), $location_info['name'] ) !!}
    </h4>
    <a class="d-none d-lg-block" href="#changeLocationModal" data-toggle="modal" data-target="#changeLocationModal">
      {!! _e('Change Location','sage') !!}
    </a>
    <button class="close-mobile d-lg-none" type="button" data-toggle="dropdown">
      <i class="far fa-times-circle"></i>
    </button>
  </div>
  <div class="dropdown-divider"></div>
  {{-- Address --}}
  <div class="dropdown-section">
    <i class="fas fa-map-marker-alt"></i>
    {!! $location_info['address_single_line'] !!}
  </div>
  <div class="dropdown-divider"></div>
  {{-- Get Directions --}}
  <div class="dropdown-section">
    <i class="fas fa-directions"></i>
    <a href="{{ $location_info['directions_uri'] }}" target="_blank">
      {!! _e('Get directions','sage') !!}
    </a>
  </div>
  <div class="dropdown-divider"></div>
  {{-- Hours --}}
  <div class="dropdown-section">
    <i class="fas fa-clock"></i>
    {{ $location_info['hours'] }}
  </div>
  <div class="dropdown-divider"></div>
  {{-- Phone Number --}}
  <div class="dropdown-section">
    <i class="fas fa-phone-alt"></i>
    <a href="#">{{ $location_info['phone'] }}</a>
  </div>
  <div class="dropdown-divider"></div>
  {{-- Email Address --}}
  <div class="dropdown-section">
    <i class="fas fa-envelope"></i>
    <a href="#">{{ $location_info['email'] }}</a>
  </div>
  <div class="dropdown-divider"></div>
  <a class="d-lg-none change-location-link-mobile" href="#changeLocationModal" data-toggle="modal" data-target="#changeLocationModal">
    {!! _e('Change My WMB Location','sage') !!}
  </a>
</div>
