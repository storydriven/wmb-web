<section class="services-offered container">
  <hr>
  <h2>{{ $services_offered['headline'] }}</h2>
  <p class="body-copy services-offered-intro">{{ $services_offered['intro'] }}</p>
  <div class="services">
    @foreach ($services_offered['services'] as $service)
      <div class="service">
        <h3>{{ $service['name'] }}</h3>
        <p class="body-copy">{{ $service['description'] }}</p>
      </div>
    @endforeach
  </div>
</section>
