<section class="intro">
  <div class="container">
    <h1>{!! _e('Contact WMB','sage') !!} {{ $location_info['name'] }}</h1>
    <div class="intro-copy">
      {!! $intro['copy'] !!}
    </div>
    <hr>
  </div>
</section>
