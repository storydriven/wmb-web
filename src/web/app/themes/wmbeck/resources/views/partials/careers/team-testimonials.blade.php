<section class="team-testimonials container">
  <div class="section-inner">
    <hr>
    <h1>{{ $team_testimonials['headline'] }}</h1>
    <div class="slider-outer">
      @component('components.slider-arrow-left')@endcomponent
      <div class="team-testimonials-slider">
        @foreach ($team_testimonials['testimonials'] as $key => $testimonial)
          <div class="testimonial">
            <div class="quote">
              &ldquo;{{ $testimonial['quote'] }}&rdquo;
            </div>
            <div class="attribution">
              {{ $testimonial['name'] }}, {{ $testimonial['position'] }}
            </div>
          </div>
        @endforeach
      </div>
      @component('components.slider-arrow-right')@endcomponent
    </div>
  </div>
</section>

@push('footer_scripts_document_ready')
  $('.team-testimonials-slider').slick({
    arrows: true,
    dots: true,
    prevArrow: $('.team-testimonials .wmb-slider-controls.prev .wmb-slider-button'),
    nextArrow: $('.team-testimonials .wmb-slider-controls.next .wmb-slider-button')
  });
@endpush
