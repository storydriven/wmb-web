<section class="coverages" id="coveragesApp">
  <div class="container">
    <div class="row">
      <div class="col header-column">
        <h1>{!! _e('Insurance Coverage For Every Need','sage') !!} <span class="coverages-count d-none d-md-inline">(@{{ coveragesCount }})</span></h1>
        <button class="btn btn-white d-none d-lg-block sort-dropdown-button" type="button" name="sortDropdownButton">A-Z</button>
      </div>
    </div>
    <div class="row d-lg-none">
      <div class="col mobile-filter-button-container">
        <div class="results-count">@{{ coveragesCount }} Results</div>
        <button class="btn btn-white show-filter-button" type="button" name="show-filter-panel" @click="showFilterPanel">{!! _e('Filter','sage') !!}</button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 filter-sidebar-column">
        <coverages-filter-sidebar
          :coverage-terms="coverageTerms"
          :checked-terms.sync="checkedTerms"
          v-on:clear-filter="clearFilterTerms"
          v-on:close-filter-panel="closeFilterPanel"
          :class="{ show: filterPanelIsVisible }"
        ></coverages-filter-sidebar>
      </div>
      <div class="col-12 col-lg-9">
        <coverages-list :coverages="coveragesToShow"></coverages-list>
      </div>
    </div>
  </div>
</section>
