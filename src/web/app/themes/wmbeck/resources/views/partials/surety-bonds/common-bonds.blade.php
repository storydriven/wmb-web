<section class="common-bonds container">
  <hr>
  <h2>{{ $common_bonds['headline'] }}</h2>
  <p class="body-copy common-bonds-intro">{{ $common_bonds['intro'] }}</p>
  <div class="bond-types">
    @foreach ($common_bonds['bond_types'] as $bond_type)
      <div class="bond-type">
        <h3>{{ $bond_type['name'] }}</h3>
        <p class="body-copy">{{ $bond_type['description'] }}</p>
      </div>
    @endforeach
  </div>
</section>
