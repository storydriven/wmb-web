<section class="wmb-difference container">
  <div class="section-inner">
    <hr>
    <h2>{{ $wmb_difference['headline'] }}</h2>
    <div class="items">
      @foreach ($wmb_difference['items'] as $item)
        <div class="item">
          <div class="item-thumbnail" style="background-image:url('{{ $item['image'] }}')">
            <div class="image-size-element"></div>
          </div>
          <h3>{{ $item['name'] }}</h3>
          <p class="body-copy">{{ $item['description'] }}</p>
        </div>
      @endforeach
    </div>
  </div>
</section>
