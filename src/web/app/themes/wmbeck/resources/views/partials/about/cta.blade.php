<section class="cta-section">
  <div class="container">
    <h2>
      {!! sprintf( __( 'Contact our %s team', 'sage' ), $location_info['name'] ) !!}
    </h2>
    <div class="copy">
      {!! $call_to_action['copy'] !!}
    </div>
    @if ($call_to_action['buttons'])
      <div class="buttons">
        @foreach ($call_to_action['buttons'] as $button)
          <a href="{!! $button['button']['url'] !!}" class="btn btn-default" @if ($button['button']['target'])target="{{ $button['button']['target'] }}"@endif>
            {{ $button['button']['title'] }}
          </a>
        @endforeach
      </div>
    @endif
  </div>
</section>
