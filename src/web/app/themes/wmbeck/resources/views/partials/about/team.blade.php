<section class="team">
  <div class="container">
    <h2>Meet the {{ $location_info['name'] }} Team</h2>
    <p class="body-copy team-intro">{{ $team['team_section_intro'] }}</p>
    <div class="team-members-outer">
      <div class="team-members">
        @foreach ($team['team_members'] as $team_member)
          <div class="team-member">
            <img src="{{ $team_member['photo'] }}" alt="{{ $team_member['name'] }}">
            <div class="department">{{ $team_member['department'] }}</div>
            <div class="name">{{ $team_member['name'] }}</div>
            <div class="title">{{ $team_member['title'] }}</div>
            @if ($team_member['email'])
              <div class="email">
                <a href="mailto:{{ $team_member['email'] }}">
                  <i class="fas fa-envelope"></i>
                </a>
              </div>
            @endif
          </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
