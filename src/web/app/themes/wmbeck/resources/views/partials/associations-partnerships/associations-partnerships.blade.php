<section class="associations-partnerships-main">
  <div class="container">
    @if ( !empty($home_builders_associations) )
      <section class="associations-partnerships-section home-builders-associations">
        <h2>{!! _e('Home Builders&apos; Associations','sage') !!}</h2>
        <ul class="organization-list">
          @foreach ($home_builders_associations as $key => $organization)
            <li>
              @if ( $organization['url'] )
                <a href="{!! $organization['url'] !!}" target="_blank">
                  <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
                </a>
              @else
                <div class="image-container">
                  <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
                </div>
              @endif
            </li>
          @endforeach
        </ul>
      </section>
    @endif

    @if ( !empty($construction_associations) )
      <section class="associations-partnerships-section construction-associations">
        <h2>{!! _e('Construction Associations','sage') !!}</h2>
        <ul class="organization-list">
          @foreach ($construction_associations as $key => $organization)
            <li>
              @if ( $organization['url'] )
                <a href="{!! $organization['url'] !!}" target="_blank">
                  <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
                </a>
              @else
                <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
              @endif
            </li>
          @endforeach
        </ul>
      </section>
    @endif

    @if ( !empty($contractor_associations) )
      <section class="associations-partnerships-section contractor-associations">
        <h2>{!! _e('Contractor Associations','sage') !!}</h2>
        <ul class="organization-list">
          @foreach ($contractor_associations as $key => $organization)
            <li>
              @if ( $organization['url'] )
                <a href="{!! $organization['url'] !!}" target="_blank">
                  <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
                </a>
              @else
                <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
              @endif
            </li>
          @endforeach
        </ul>
      </section>
    @endif

    @if ( !empty($valued_partnerships) )
      <section class="associations-partnerships-section valued-partnerships">
        <h2>{!! _e('Valued Partnerships','sage') !!}</h2>
        <ul class="organization-list">
          @foreach ($valued_partnerships as $key => $organization)
            <li>
              @if ( $organization['url'] )
                <a href="{!! $organization['url'] !!}" target="_blank">
                  <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
                </a>
              @else
                <img src="{!! $organization['logo'] !!}" alt="{{ $organization['name'] }}">
              @endif
            </li>
          @endforeach
        </ul>
      </section>
    @endif
  </div>
</section>
