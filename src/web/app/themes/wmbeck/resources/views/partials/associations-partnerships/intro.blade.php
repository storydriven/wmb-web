<section class="intro">
  <div class="container">
    <h1>WMB {{ $location_info['name'] }} Associations & Partnerships</h1>
    <p class="body-copy">{!! _e('We partner with the following associations.','sage') !!}</p>
  </div>
</section>
