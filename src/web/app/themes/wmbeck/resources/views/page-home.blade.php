@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Hero --}}
    @include('partials.home.hero')
    {{-- Intro --}}
    @include('partials.home.intro')
    {{-- Popular Coverages --}}
    @include('partials.global.popular-coverages')
    {{-- Testimonials --}}
    @include('partials.global.testimonials')
    {{-- Community Updates --}}
    @include('partials.home.community-updates')
    {{-- CTA --}}
    @include('partials.global.cta')
  @endwhile

  @include('partials.modals.request-quote')
@endsection
