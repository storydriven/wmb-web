@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post(); @endphp
    {{-- Hero --}}
    @include('partials.global.hero')
    {{-- Intro --}}
    @include('partials.global.intro')
    {{-- The WMB Difference --}}
    @include('partials.careers.wmb-difference')
    {{-- Team Testimonials --}}
    @include('partials.careers.team-testimonials')
    {{-- CTA --}}
    @include('partials.global.cta')
  @endwhile
@endsection
