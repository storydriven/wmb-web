import Vue from 'vue'
import CoveragesFilterSidebar from './vue/components/coverages-filter-sidebar.vue'
import CoveragesList from './vue/components/coverages-list.vue'

new Vue({
  el: '#coveragesApp',
  components: {
    CoveragesFilterSidebar,
    CoveragesList,
  },
  data: {
    // eslint-disable-next-line no-undef
    coverages: coveragesData,
    // eslint-disable-next-line no-undef
    coverageTerms: coveragesTaxonomyData,
    checkedTerms: [],
    filterPanelIsVisible: false
  },
  computed: {
    coveragesToShow: function() {
      window.checkedTerms = this.checkedTerms
      if ( window.checkedTerms.length ) {
        let filteredCoverages = this.coverages.filter( function(e) {
          let showThisItem = false
          e.terms.forEach((term) => {
            if ( window.checkedTerms.includes(term) )
              showThisItem = true
          })
          return  showThisItem
        } )
        return this.sortCoverages(filteredCoverages)
      } else {
        return this.sortCoverages(this.coverages)
      }
    },
    coveragesCount: function() {
      return this.coverages.length
    }
  },
  methods: {
    sortCoverages: function(coverages) {
      return coverages.sort( (a,b) => ( a.name > b.name) ? 1 : -1  )
    },
    showFilterPanel: function() {
      if ( !this.filterPanelIsVisible )
        this.filterPanelIsVisible = true
    },
    closeFilterPanel: function() {
      if ( this.filterPanelIsVisible )
        this.filterPanelIsVisible = false
    },
    clearFilterTerms: function() {
      this.checkedTerms = []
      if ( this.filterPanelIsVisible )
        this.filterPanelIsVisible = false
    }
  },
  mounted() {},
})
