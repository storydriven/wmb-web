// import external dependencies
import 'jquery';

import { library, dom } from '@fortawesome/fontawesome-svg-core';

import {
  faChevronLeft,
  faChevronRight,
  faChevronDown,
  faSearch,
  faMapMarkerAlt,
  faDirections,
  faClock,
  faPhoneAlt,
  faEnvelope,
  faUser
} from '@fortawesome/free-solid-svg-icons';

import {
  faTimesCircle
} from '@fortawesome/free-regular-svg-icons';

import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faTwitter
} from '@fortawesome/free-brands-svg-icons';

library.add(
  faChevronLeft,
  faChevronRight,
  faChevronDown,
  faTimesCircle,
  faSearch,
  faMapMarkerAlt,
  faDirections,
  faClock,
  faPhoneAlt,
  faEnvelope,
  faUser,
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faTwitter
);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
