import * as Cookies from 'js-cookie';
import 'waypoints/lib/noframework.waypoints.min.js';

export default {
  init() {},
  finalize() {
    /**
    * Change user's selected branch.
    * Sends ajax request to server, to confirm if requested location exists in WordPress.
    * If request is successful, wmb_user_branch cookie is updated, and the page is reloaded.
    **/
    $('.modal.change-location .change-location-button').on('click', function(e) {
      let changeToLocation = $(e.target).data('location');
      $.ajax({
        // eslint-disable-next-line no-undef
        url: themeData.siteUrl + '/wp-json/wmb/v1/locations/change',
        data: {
          changeToLocation: changeToLocation,
        },
        success: (response) => {
          Cookies.set('wmb_user_branch', response, { expires: 100 });
          location.reload();
        },
        error: (error) => {
          console.log('Ajax Error');
          console.log(error);
        },
      });
    });

    // Shrinks navbar when user scrolls down from top of page
    /* eslint-disable */
    var navWaypoint = new Waypoint({
      element: document.body,
      offset: 0 - ( window.innerHeight * 0.1 ),
      handler: function(direction) {
        const navbar = document.querySelector('header.banner');
        if (direction == "down") {
          navbar.classList.add('compact');
        } else {
          navbar.classList.remove('compact');
        }
      }
    });
    /* eslint-enable */
  },
};
