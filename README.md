# [DockPress](https://joepurdy.io/words/dockerize-wordpress-with-roots.io)  

DockPress is a dockerized version of WordPress inspired heavily by the Roots.io approach to managing a modern WordPress application.  

## Features  

* Pre-defined development workflow using a single script  
* Support for simple initialization of [Bedrock](https://roots.io/bedrock/) and [Sage](https://roots.io/sage/)  
* Composer package management  
* WP-CLI commands for easier administration  
* Better folder structure  
* Dependency management with [Composer](http://getcomposer.org)   
* Easy WordPress configuration with environment specific files  
* Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)  
* Autoloader for mu-plugins (use regular plugins as mu-plugins)  
* Enhanced security (separated web root and secure passwords with [wp-password-bcrypt](https://github.com/roots/wp-password-bcrypt))  

## Requirements  

* Docker - [Install](https://www.docker.com/community-edition#/download)  

## Development Installation

1. Update docker-compose, ports and env values. (https://roots.io/salts.html for random salts)  

2. Run the initialization command with the develop script:  
  `./develop init {theme-name}` (existing theme name will just init the current theme)  

5. Follow the prompts which create the package.json and allow base package Installation. When enquired about the url use the same as you .env file including port. NB: use bootstrap and font-awesome for consistency.  

4. Update theme url and directory in `develop` bash up script for npm and DockerFile.

4. Bring the Docker environment online with `./develop up -d --no-recreate`  

5. Create an entry in your hostfile mapping the `WP_HOME` address to `127.0.0.1`  
  e.g. /etc/hosts - 127.0.0.1 {theme-name}.test  

6. Access WP admin at `http://localhost:3000/wp/wp-admin` (browsersync url can be modified at /src/web/app/themes/{theme-name}/resources/assets/config.json)

7. To add additional themes after initialisation simply run `composer create-project roots/sage {theme-name} dev-master` inside the `src/web/app/themes` folder  

## Development  
1. `./develop up -d --no-recreate`  

2. Look at district-group for a good theme layout and build. Keep the logic seperate from blade templating and use blade templating syntax. ES6 javascript syntax. SCSS styling.

3. Setup is preloaded with composer plugins. Only install/activate required plugins. Add additional plugins if you can confirm are good, fast, valuable and will be used in other projects. All plugins/wordpress version are handled by composer.

4. Look at the roots documentation for themes (https://roots.io/sage/)

5. On save webpack will provide feedback for errors, etc and browsersync will update the page automatically.

## Staging Preperation

1. Update /envs/staging files and rename from '.example'. (https://roots.io/salts.html)

2. Update docker-compose.staging.yml file and rename from '.example'

3. Update bitbucket-pipelines.yml file to include pipelines and rename from '.example'

## Staging Deploy

4. Merge into Staging  

## Production  Preperation

1. Update /envs/production files and rename from '.example'. (https://roots.io/salts.html)

2. Update docker-compose.production.yml file and rename from '.example'

3. Update bitbucket-pipelines.yml file to include pipelines and rename from '.example'

## Production  Deploy

1. Merge to Production  

## Common libraries (use for project consistency)  
Bootstrap  
Font-Awesome  
Slick Carousel  
Google Analytics  

## Contributing  

Contributions are welcome from everyone. I don't consider myself an expert here since I only work on WordPress projects once in a blue moon. Check out the [contributing guidelines](https://github.com/joepurdy/DockPress/blob/master/CONTRIBUTING.md) to help you get started.
